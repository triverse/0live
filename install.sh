#!/bin/sh

. /library/libpath

for i in $@; do
    case "$i" in
        -h|--help)
            echo "usage: $0 (rootdir=<directory>)"
            exit 0;;
        rootdir=*)
            ROOTDIR=${i#*=};;
    esac
done

install -v -Dm755 live0.sh $ROOTDIR/$bindir/live0

install -v -Dm644 0live.cfg $ROOTDIR/$cfgdir/0live

install -v -Dm644 grub.cfg $ROOTDIR/$datdir/0live/grub.cfg
install -v -Dm644 isolinux.cfg $ROOTDIR/$datdir/0live/isolinux.cfg
install -v -m644 install.txt $ROOTDIR/$datdir/0live/install.txt

sed -i -e "s,/kernel,$krndir,g" $ROOTDIR/$datdir/0live/install.txt
sed -i -e "s,/configure,$cfgdir,g" $ROOTDIR/$datdir/0live/install.txt
sed -i -e "s,/library,$libdir,g" $ROOTDIR/$datdir/0live/install.txt
