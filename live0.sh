#!/bin/sh

. /library/libpath
. $cfgdir/0live

set_live_default() {
    chroot work/rootfs $bindir/sh -c "initctl enable dhcpcd"

    if [ "$_ULTRA_" = 1 ]; then
        chroot work/rootfs $bindir/sh -c "initctl enable elogind"
    fi
}

make_live0() {
    iso_version=$(date +%Y.%m.%d)
    iso_name="live0infra-$iso_version.iso"

    if [ "$_ULTRA_" = 1 ]; then
        iso_name="live0ultra-$iso_version.iso"
    fi

    iso_label="0_$(date +%Y%m)"
    iso_publisher="0 <http://www.xyz.org>"
    iso_application="0 Live/Rescue CD"

    mkdir -p work/rootfs/livecd
    mkdir -p work/rootfs/rootfs

    if [ "$_ULTRA_" = 1 ]; then
        add0 ${pkgs_ultra[@]} rootdir=work/rootfs
    else
        add0 ${pkgs_infra[@]} rootdir=work/rootfs
    fi

    set_live_default
    chroot work/rootfs $bindir/sh -c "echo \"For the installation instructions,\" >> $cfgdir/motd"
    chroot work/rootfs $bindir/sh -c "echo \"read the /install.txt file\" >> $cfgdir/motd"
    cp $datdir/0live/install.txt work/rootfs

    mkdir -p work/iso/Live0
    mksquashfs work/rootfs work/iso/Live0/rootfs.sfs \
        -noappend -comp xz -no-progress -b 1M -Xdict-size 100%

    mkdir work/iso/isolinux
    cp $libdir/syslinux/bios/isolinux.bin work/iso/isolinux
    cp $libdir/syslinux/bios/isohdpfx.bin work/iso/isolinux
    cp $libdir/syslinux/bios/ldlinux.c32 work/iso/isolinux
    cp $libdir/syslinux/bios/vesamenu.c32 work/iso/isolinux
    cp $libdir/syslinux/bios/libcom32.c32 work/iso/isolinux
    cp $libdir/syslinux/bios/libutil.c32 work/iso/isolinux
    sed "s|live0|$iso_label|g" \
        $datdir/0live/isolinux.cfg > work/iso/isolinux/isolinux.cfg

    cp $krndir/boot/vmlinuz work/iso/isolinux
    mkinitramfs $kver work/iso/isolinux/initramfs

    truncate -s 10M work/iso/isolinux/efiboot.img
    mkdosfs -n LIVE0_EFI work/iso/isolinux/efiboot.img

    mkdir -p work/efiboot
    mount work/iso/isolinux/efiboot.img work/efiboot

    mkdir -p work/efiboot/EFI/{boot,fonts}
    cp $krndir/boot/efi/gnulin/{boot,grub}x64.efi work/efiboot/EFI/boot
    cp $krndir/boot/efi/gnulin/fonts/unicode.pf2 work/efiboot/EFI/fonts
    sed "s|live0|$iso_label|g" \
        $datdir/0live/grub.cfg > work/efiboot/EFI/boot/grub.cfg

    umount -d work/efiboot

    mkdir -p work/iso/EFI/{boot,fonts}
    cp $krndir/boot/efi/gnulin/{boot,grub}x64.efi work/iso/EFI/boot
    cp $krndir/boot/efi/gnulin/fonts/unicode.pf2 work/iso/EFI/fonts
    sed "s|live0|$iso_label|g" \
        $datdir/0live/grub.cfg > work/iso/EFI/boot/grub.cfg

    xorriso \
        -as mkisofs \
        -iso-level 3 \
        -full-iso9660-filenames \
        -volid "$iso_label" \
        -appid "$iso_application" \
        -publisher "$iso_publisher" \
        -preparer "prepared by live0" \
        -eltorito-boot isolinux/isolinux.bin \
        -eltorito-catalog isolinux/boot.cat \
        -no-emul-boot -boot-load-size 4 -boot-info-table \
        -isohybrid-mbr work/iso/isolinux/isohdpfx.bin \
        -eltorito-alt-boot \
        -e isolinux/efiboot.img \
        -no-emul-boot \
        -isohybrid-gpt-basdat \
        -output "$iso_name" \
        work/iso

    rm -r work
}

_ULTRA_=0

for _grp in ${grps[@]}; do
    if [ "$_grp" = "infra" ]; then
        pkgs_infra+=(${pkgs[@]} infra)
    fi

    if [ "$_grp" = "infra-dev" ]; then
        pkgs_infra+=(infra-dev)
    fi

    if [ "$_grp" = "ultra" ]; then
        _ULTRA_=1
        pkgs_ultra=(${pkgs[@]} ${grps[@]})
    fi
done

if [ "$_ULTRA_" = 1 ]; then
    make_live0
    _ULTRA_=0
    make_live0
else
    make_live0
fi
